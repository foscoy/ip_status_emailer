I originally wrote this to be able to retrieve the IP address of my
RaspberryPi from anywhere without having to set up a domain or other
form of static IP service. This script is meant to be ran in the
background on a Linux OS. 

The way it works is that you E-mail an account setup up for the
raspberrypi (or any other system) and it will respond back with the
public IP address of the network the system is on.

The script checks for a specific email address and subject line. If
conditions are met, it will respond with the information requested.
If conditions are not met, it will ignore the email message.

This script can be modified to return any other information that can
be gathered through the system. 

Requirements
=============
Linux OS
Python 3.x
pyzmail
pprint

Instructions
=============
1. Run script using a TMUX/Screen or any other system that allows you
   keep the program running with out having to stay logged in.
2. Enter account information for the email address you wish to use.
3. Using any other email(or even the same email) send an email with
   the subject line "Where are you?"
4. Wait for the response...

Notes
=============
This can be made simpler by instead running a cron job to periodically
check the email.

* I still need to add contingencies to deal with errors but the script is 
  working. (As far as I know...)
#!/usr/bin/env python3
import os
import imapclient
import pyzmail
import smtplib
import pprint
import time

# Gets ip address using linux command line and returns the string.
def get_ip():
    ip_add = os.popen('dig TXT +short o-o.myaddr.l.google.com @ns1.google.com')
    return ip_add.read()

# Reads email and checks for sender
def read_email(login, rcp):
    imapObj = imapclient.IMAPClient('imap.gmail.com', ssl=True) #setup fo Gmail
    try:
        imapObj.login(login['pc_user'], login['pc_pswd'])
    except:
        return 2
    imapObj.select_folder('INBOX', readonly=False)
    UIDs = imapObj.search(['UNSEEN'])
    rawMessage = imapObj.fetch(UIDs, ['BODY[]'])
    try:
        message = pyzmail.PyzMessage.factory(rawMessage[UIDs[len(UIDs)-1]][b'BODY[]'])
    except:
        return 3
    if message.get_address('from')[1] == rcp and message.get_subject() == 'Where are you?':
        imapObj.logout()
        return 0
    else:
        iampObj.logout()
        return 1

# Sends email
def send_email(login, rcp, ip):
    smtpObj = smtplib.SMTP('smtp.gmail.com', 587) #setup for Gmail
    if smtpObj.ehlo()[0] != 250:
        return "Error"
    smtpObj.starttls()
    smtpObj.login(login['pc_user'], login['pc_pswd'])
    smtpObj.sendmail(login['pc_user'], rcp,
                    'Subject: Here I am!\n'+ip)
    smtpObj.quit()

# Gather information from user
login = {'pc_user':'b', 'pc_pswd':'b'}
login['pc_user'] = input("Please input PC's email: ")
login['pc_pswd'] = input("Please input PC's email password: ")
rcp = input("Please input email to send info to: ")

while True:
    if read_email(login, rcp) == 0:
        print("sending email")
        send_email(login, rcp, get_ip())
        time.sleep(60)
    elif read_email(login, rcp) == 1:
        print("email not from user")
        time.sleep(60)
    elif read_email(login, rcp) == 2:
        print("login error")
        time.sleep(60)
    elif read_email(login, rcp) == 3:
        print("no new email")
        time.sleep(60)
    else:
        print("error")
        time.sleep(60)
        